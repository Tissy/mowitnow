package com.cedm.mower.model;

import com.google.common.collect.Lists;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class MowerTest {

    @Test
    public void testExecuteOrderLeft() throws Exception {
        Mower mower = new Mower(new Position(0 , 0, Direction.NORTH), 1, 1);

        Position position = mower.executeOrders(Lists.newArrayList(Order.GAUCHE));

        assertThat(position.getDirection(), is(Direction.WEST));
    }

    @Test
    public void testExecuteOrderRight() throws Exception {
        Mower mower = new Mower(new Position(0 , 0, Direction.NORTH), 1, 1);

        Position position = mower.executeOrders(Lists.newArrayList(Order.DROITE));

        assertThat(position.getDirection(), is(Direction.EAST));
    }

    @Test
    public void testExecuteMoveUp() throws Exception {
        Mower mower = new Mower(new Position(0 , 0, Direction.NORTH), 1, 1);

        Position position = mower.executeOrders(Lists.newArrayList(Order.AVANCE));

        assertThat(position.getX(), is(0));
        assertThat(position.getY(), is(1));
    }

    @Test
    public void testExecuteMoveDown() throws Exception {
        Mower mower = new Mower(new Position(1 , 1, Direction.SOUTH), 1, 1);

        Position position = mower.executeOrders(Lists.newArrayList(Order.AVANCE));

        assertThat(position.getX(), is(1));
        assertThat(position.getY(), is(0));
    }

    @Test
         public void testExecuteMoveLeft() throws Exception {
        Mower mower = new Mower(new Position(1 , 1, Direction.WEST), 1, 1);

        Position position = mower.executeOrders(Lists.newArrayList(Order.AVANCE));

        assertThat(position.getX(), is(0));
        assertThat(position.getY(), is(1));
    }

    @Test
    public void testExecuteMoveRight() throws Exception {
        Mower mower = new Mower(new Position(0, 0, Direction.EAST), 1, 1);

        Position position = mower.executeOrders(Lists.newArrayList(Order.AVANCE));

        assertThat(position.getX(), is(1));
        assertThat(position.getY(), is(0));
    }

    @Test
    public void testExecuteOutsideOfLawnFromBottom() throws Exception {
        Mower mower = new Mower(new Position(0 , 0, Direction.SOUTH), 1, 1);

        Position position = mower.executeOrders(Lists.newArrayList(Order.AVANCE));

        assertThat(position.getX(), is(0));
        assertThat(position.getY(), is(0));
    }
    @Test
    public void testExecuteOutsideOfLawnFromTop() throws Exception {
        Mower mower = new Mower(new Position(1 , 1, Direction.NORTH), 1, 1);

        Position position = mower.executeOrders(Lists.newArrayList(Order.AVANCE));

        assertThat(position.getX(), is(1));
        assertThat(position.getY(), is(1));
    }

    @Test
    public void testExecuteOutsideOfLawnFromLeft() throws Exception {
        Mower mower = new Mower(new Position(0 , 0, Direction.WEST), 1, 1);

        Position position = mower.executeOrders(Lists.newArrayList(Order.AVANCE));

        assertThat(position.getX(), is(0));
        assertThat(position.getY(), is(0));
    }

    @Test
    public void testExecuteOutsideOfLawnFromRight() throws Exception {
        Mower mower = new Mower(new Position(1 , 1, Direction.EAST), 1, 1);

        Position position = mower.executeOrders(Lists.newArrayList(Order.AVANCE));

        assertThat(position.getX(), is(1));
        assertThat(position.getY(), is(1));
    }
}