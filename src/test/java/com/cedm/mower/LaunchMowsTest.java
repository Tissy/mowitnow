package com.cedm.mower;

import com.cedm.mower.exception.PathNotFoundException;
import com.cedm.mower.parse.ConfigurationParser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class LaunchMowsTest {

    @Mock
    private ConfigurationParser parser;
    @Mock
    private ControlService controlService;

    @InjectMocks
    private LaunchMows tested;

    @Test(expected = PathNotFoundException.class)
    public void testStartWithNullArgs() throws Exception {
        tested.start(null);
    }

    @Test(expected = PathNotFoundException.class)
    public void testStartWithEmptyArgs() throws Exception {
        tested.start(new String[]{});
    }
}