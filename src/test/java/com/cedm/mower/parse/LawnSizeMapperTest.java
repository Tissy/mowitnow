package com.cedm.mower.parse;

import com.cedm.mower.model.LawnSize;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class LawnSizeMapperTest {

    @InjectMocks
    private LawnMapper tested;

    @Test(expected = IllegalArgumentException.class)
    public void testMapThrowErrorWhenLineDoesntMatch() throws Exception {
        tested.map("not valid");
    }

    @Test
    public void testMap() throws Exception {
        LawnSize result = tested.map("11 12");

        assertThat(result.getWidth(), is(11));
        assertThat(result.getHeight(), is(12));
    }
}