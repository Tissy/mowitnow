package com.cedm.mower.parse;

import com.cedm.mower.model.Direction;
import com.cedm.mower.model.MowerConfiguration;
import com.cedm.mower.model.Order;
import com.google.common.collect.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ConfigurationMapperTest {

    @InjectMocks
    private ConfigurationMapper tested;

    @Test(expected = IllegalArgumentException.class)
    public void testMapThrowErrorWhenConfigurationsListIsNotAtGoodSize() throws Exception {
        tested.map(Lists.newArrayList());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMapThrowErrorWhenPositionIsNotWellFormatted() throws Exception {
        MowerConfiguration result = tested.map(Arrays.asList("33N", "GDA"));
    }

    @Test
    public void testMap() throws Exception {
        MowerConfiguration result = tested.map(Arrays.asList("3 3 N", "GDA"));

        assertThat(result.getPosition().getDirection(), is(Direction.NORTH));
        assertThat(result.getOrders(), is(Arrays.asList(Order.GAUCHE, Order.DROITE, Order.AVANCE)));
    }
}