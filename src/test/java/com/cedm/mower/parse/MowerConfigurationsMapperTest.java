package com.cedm.mower.parse;

import com.cedm.mower.model.MowerConfiguration;
import com.cedm.mower.parse.ConfigurationMapper;
import com.cedm.mower.parse.MowerConfigurationsMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MowerConfigurationsMapperTest {

    @InjectMocks
    private MowerConfigurationsMapper tested;
    @Mock
    private ConfigurationMapper mapper;

    @Test
    public void testMap() throws Exception {
        List<List<String>> list = new ArrayList<>();
        List<String> configurationsOne = mock(List.class);
        List<String> configurationsTwo = mock(List.class);
        list.addAll(asList(configurationsOne, configurationsTwo));
        final MowerConfiguration firstExpected = mock(MowerConfiguration.class);
        final MowerConfiguration secondExpected = mock(MowerConfiguration.class);
        when(mapper.map(configurationsOne)).thenReturn(firstExpected);
        when(mapper.map(configurationsTwo)).thenReturn(secondExpected);

        List<MowerConfiguration> actuals = tested.map(list);

        assertThat(actuals, is(asList(firstExpected, secondExpected)));
    }
}