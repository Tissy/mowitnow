package com.cedm.mower;

import com.cedm.mower.app.configuration.Config;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;

import java.io.IOException;


public class AppInitialization {

    public static void main(String[] args) throws IOException, InterruptedException {
        ApplicationContext applicationContext = SpringApplication.run(Config.class);
        LaunchMows launcher = applicationContext.getBean(LaunchMows.class);
        try {
            launcher.start(args);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }
        System.exit(0);
    }
}
