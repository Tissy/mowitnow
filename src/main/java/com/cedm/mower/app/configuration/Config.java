package com.cedm.mower.app.configuration;

import com.cedm.mower.LaunchMows;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration("serviceConfig")
@ComponentScan(basePackageClasses = LaunchMows.class)
public class Config {
}
