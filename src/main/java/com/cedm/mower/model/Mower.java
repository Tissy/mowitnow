package com.cedm.mower.model;

import java.util.List;

public class Mower {
    private Position position;
    private Integer maxWidth;
    private Integer maxHeight;

    public Mower(Position initialPosition, Integer maxWidth, Integer maxHeight) {
        this.position = initialPosition;
        this.maxWidth = maxWidth;
        this.maxHeight = maxHeight;
    }

    public Position getPosition() {
        return position;
    }

    public Integer getMaxWidth() {
        return maxWidth;
    }

    public Integer getMaxHeight() {
        return maxHeight;
    }

    public Position executeOrders(List<Order> orders) {
        for (Order order : orders) {
            executeOrder(order);
        }
        return position;
    }

    private void executeOrder(Order order) {
        switch (order) {
            case GAUCHE:
                position.setDirection(position.getDirection().turnAtLeft());
                break;
            case DROITE:
                position.setDirection(position.getDirection().turnAtRight());
                break;
            case AVANCE:
                tryMoving();
                break;
        }
    }

    private void tryMoving() {
        final Position nextPosition = position.defineNextPosition();
        if (canMowerGoHere(nextPosition)) {
            position.setX(nextPosition.getX());
            position.setY(nextPosition.getY());
        }
    }

    private boolean canMowerGoHere(Position nextPosition) {
        return nextPosition.getX() >= 0
                && nextPosition.getX() <= maxWidth
                && nextPosition.getY() >= 0
                && nextPosition.getY() <= maxHeight;
    }
}
