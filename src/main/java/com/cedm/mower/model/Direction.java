package com.cedm.mower.model;

public enum Direction {
    NORTH("N", 1),
    EAST("E", 2),
    SOUTH("S", 3),
    WEST("W", 4);

    private final String code;
    private final Integer sequence;

    Direction(String code, Integer sequence) {
        this.code = code;
        this.sequence = sequence;
    }

    public String getCode() {
        return code;
    }

    public static Direction codeToDirection(String code) {
        Direction[] directions = Direction.values();
        for (Direction direction : directions) {
            if(direction.getCode().equals(code)) {
                return direction;
            }
        }
        throw new IllegalArgumentException("The following charactere is not allowed for direction: " + code);
    }

    private static Direction sequenceToDirection(Integer sequence) {
        Direction[] directions = Direction.values();
        for (Direction direction : directions) {
            if(direction.sequence.equals(sequence)) {
                return direction;
            }
        }
        throw new IllegalArgumentException("The following sequence is not allowed for direction: " + sequence);
    }

    public Direction turnAtLeft() {
        return this.sequence == 1 ? Direction.WEST : Direction.sequenceToDirection(this.sequence - 1);
    }

    public Direction turnAtRight() {
        return this.sequence == 4 ? Direction.NORTH : Direction.sequenceToDirection(this.sequence + 1);
    }
}
