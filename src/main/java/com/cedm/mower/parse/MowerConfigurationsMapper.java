package com.cedm.mower.parse;

import com.cedm.mower.model.MowerConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Component
public class MowerConfigurationsMapper {

    @Autowired
    private ConfigurationMapper mapper;

    public List<MowerConfiguration> map(List<List<String>> mowersConfiguration) {
        return mowersConfiguration
                .stream()
                .map(mapper::map)
                .collect(toList());
    }
}
