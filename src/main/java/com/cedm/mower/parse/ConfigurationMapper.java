package com.cedm.mower.parse;

import com.cedm.mower.model.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@Component
public class ConfigurationMapper {

    private static final String MOWER_POSITION_PATTER = "[0-9]+ [0-9]+ [NESW]";

    public MowerConfiguration map(List<String> mowerConfiguratonSet) {
        if(mowerConfiguratonSet.size() != 2) {
            throw new IllegalArgumentException("Invalid configuration lines number");
        }
        return new MowerConfiguration(
                mapInitialPosition(mowerConfiguratonSet.get(0)),
                mapOrders(mowerConfiguratonSet.get(1)));
    }

    private Position mapInitialPosition(String position) {
        if(!Pattern.matches(MOWER_POSITION_PATTER, position)) {
            throw new IllegalArgumentException("Pattern for mower position must be: " + MOWER_POSITION_PATTER);
        }
        String[] splitted = position.split(" ");
        return new Position(Integer.parseInt(splitted[0]),
                Integer.parseInt(splitted[1]),
                Direction.codeToDirection(splitted[2]));
    }

    private List<Order> mapOrders(String orders) {
        char[] chars = orders.toCharArray();
        final List<Order> orderList = new ArrayList<>();
        for (char aChar : chars) {
            orderList.add(Order.charToAction(aChar));
        }
        return orderList;
    }
}
