package com.cedm.mower;

import com.cedm.mower.model.LawnSize;
import com.cedm.mower.model.Mower;
import com.cedm.mower.model.Position;
import org.springframework.stereotype.Component;

@Component
public class MowerFactory {

    public Mower create(final Position initialPosition, final LawnSize lawnSize) {
        return new Mower(initialPosition, lawnSize.getWidth(), lawnSize.getHeight());
    }
}
